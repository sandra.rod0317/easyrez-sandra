﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertarEasyRez.aspx.cs" Inherits="Easy_Test.InsertarEasyRez" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display&display=swap" rel="stylesheet">
    <title>Insertar Easy-Rez</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" href="Resources/css/bootstrap.min.css" rel="stylesheet" />

    <link href="Resources/css/Easyprincipal.css" rel="stylesheet" />

    <!-- Custom stlylesheet -->
    <link type="text/css" href="Resources/css/style.css" rel="stylesheet" />

</head>

<body>
    <div id="booking" class="section">
        <div class="section-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="booking-cta">
                            <h1>TEST EASY REZ</h1>

                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="booking-form">
                            <form id="ConsultaEasy" runat="server">
                                <div class="">
                                    <div class="">
                                        <h2>Sistema Prueba Easy-Rez</h2>
                                        <br />

                                        <%--Etiquetas--%>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="Razón Social:"></asp:Label>
                                                <br />
                                                <asp:TextBox runat="server" ID="RazonSocialText"></asp:TextBox>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="RFC:"></asp:Label>
                                                <br />
                                                <asp:TextBox runat="server" Text="" ID="RFCText"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="Correo:"></asp:Label>
                                                <br />
                                                <asp:TextBox runat="server" Text="" ID="TextCorreo"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">

                                            <%--Inicio de lista Tipo Persona--%>
                                            <div class="col-md-6">
                                                <asp:Label runat="server" class="eazytext" Text="Tipo de Persona:"></asp:Label>
                                                <asp:DropDownList ID="Tpersona" name="TpersonaEasy" runat="server">
                                                    <asp:ListItem Value='0'>Selecciona</asp:ListItem>
                                                    <asp:ListItem Value='119'>Persona Fisica</asp:ListItem>
                                                    <asp:ListItem Value='120'>Persona Moral</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <%--Radio Botton Sucursal--%>
                                            <div class="col-md-6">
                                                <asp:RadioButton runat="server" ID="SucursalRadio"></asp:RadioButton>
                                                <asp:Label runat="server" class="eazytext" Text="Es Sucursal"></asp:Label>

                                            </div>
                                        </div>

                                        <br />

                                        <%--Inicio de lista Entidad Tributaria--%>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:Label runat="server" class="eazytext" Text="Tipo Entidad Tributaria:"></asp:Label>
                                                <asp:DropDownList ID="TipoETributaria" name="TipoETributaria" runat="server">
                                                    <asp:ListItem Value='0'>Selecciona</asp:ListItem>
                                                    <asp:ListItem Value='117'>Cliente</asp:ListItem>
                                                    <asp:ListItem Value='118'>Emisor</asp:ListItem>
                                                    <asp:ListItem Value='126'>Proveedor PAC</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <br />

                                        <%--Inicio de lista Regimen Fiscal--%>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:Label runat="server" class="eazytext" Text="Regimen Fiscal:"></asp:Label>
                                                <asp:DropDownList ID="RegimenFiscalLista" name="RegimenFiscalLista" runat="server">
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <br />
                                        <div class="row">
                                            <%--Inicio de Lista CFDI--%>
                                            <div class="col-md-6">
                                                <asp:Label runat="server" class="eazytext" Text="Uso CFDI:"></asp:Label>
                                                <asp:DropDownList ID="CFDILista" name="CFDILista" runat="server">
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <br />

                                        <%--Boton Cancelar--%>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:Button runat="server" class="boton" Text="Cancelar"></asp:Button>
                                            </div>

                                            <%--Boton Guardar--%>
                                            <div class="col-md-6">
                                                <asp:Button runat="server" class="boton" OnClick="Guardar" Text="Guardar"></asp:Button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




</body>


</html>

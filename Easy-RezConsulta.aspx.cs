﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;

namespace Easy_Test
{
    public partial class Easy_RezConsulta : System.Web.UI.Page
    {

        public DataTable infobase = new DataTable();
        public int rows;
        public int rowposition;
        string ConexionBase = @"Server=localhost;Database=easyrez-sandra;Uid=root;Pwd=SandraRod1601";
        protected void Page_Load(object sender, EventArgs e)
        {
           
            using (MySqlConnection sqlcon = new MySqlConnection(ConexionBase))
            {
                sqlcon.Open();
                MySqlDataAdapter sqlda = new MySqlDataAdapter("BusquedaEntidad", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.AddWithValue("idclas", Convert.ToInt32(117));
              
                sqlda.Fill(infobase);
                rows = infobase.Rows.Count;
                rowposition = 0;


            }
            //Validación-información siguiente
            TipoPersonaEasy.Text = infobase.Rows[0]["TipoPersona"].ToString();
            RazonSocialLabel.Text = infobase.Rows[0]["RazonSocial"].ToString();
            RFCLabel.Text = infobase.Rows[0]["RFC"].ToString();
            CorreoLabel.Text = infobase.Rows[0]["correo"].ToString();
            MetodoPago.Text = infobase.Rows[0]["Metodo Pago"].ToString();
            UsoCFDIClaveLabel.Text = infobase.Rows[0]["UsoCFDIClave"].ToString();
            UsoCFDIDesLabel.Text = infobase.Rows[0]["UsoCDFIDes"].ToString();
            TipoPersonaLabel.Text = infobase.Rows[0]["TipodePersona"].ToString();
            RegimenLabelClave.Text = infobase.Rows[0]["RegimenFiscalClave"].ToString();
            RegimenLabelDes.Text = infobase.Rows[0]["RegimenFiscalDes"].ToString();
        
        }
        public void SiguienteEazy(object sender, EventArgs e)
        {
             rowposition= rowposition+1;
            if((rows-1) <= rowposition){
                TipoPersonaEasy.Text = infobase.Rows[rowposition]["TipoPersona"].ToString();
                RazonSocialLabel.Text = infobase.Rows[rowposition]["RazonSocial"].ToString();
                RFCLabel.Text = infobase.Rows[rowposition]["RFC"].ToString();
                CorreoLabel.Text = infobase.Rows[rowposition]["correo"].ToString();
                MetodoPago.Text = infobase.Rows[rowposition]["Metodo Pago"].ToString();
                UsoCFDIClaveLabel.Text = infobase.Rows[rowposition]["UsoCFDIClave"].ToString();
                UsoCFDIDesLabel.Text = infobase.Rows[rowposition]["UsoCDFIDes"].ToString();
                TipoPersonaLabel.Text = infobase.Rows[rowposition]["TipodePersona"].ToString();
                RegimenLabelClave.Text = infobase.Rows[rowposition]["RegimenFiscalClave"].ToString();
                RegimenLabelDes.Text = infobase.Rows[rowposition]["RegimenFiscalDes"].ToString();
            }
            else{
                rowposition = 0;
                TipoPersonaEasy.Text = infobase.Rows[0]["TipoPersona"].ToString();
                RazonSocialLabel.Text = infobase.Rows[0]["RazonSocial"].ToString();
                RFCLabel.Text = infobase.Rows[0]["RFC"].ToString();
                CorreoLabel.Text = infobase.Rows[0]["correo"].ToString();
                MetodoPago.Text = infobase.Rows[0]["Metodo Pago"].ToString();
                UsoCFDIClaveLabel.Text = infobase.Rows[0]["UsoCFDIClave"].ToString();
                UsoCFDIDesLabel.Text = infobase.Rows[0]["UsoCDFIDes"].ToString();
                TipoPersonaLabel.Text = infobase.Rows[0]["TipodePersona"].ToString();
                RegimenLabelClave.Text = infobase.Rows[0]["RegimenFiscalClave"].ToString();
                RegimenLabelDes.Text = infobase.Rows[0]["RegimenFiscalDes"].ToString();
            }
           
        }
        protected void AnteriorEazy(object sender, EventArgs e)
        {
            //Validación
            rowposition= rowposition- 1;
            if ( rowposition < 0)
            {

                rowposition = 0;
                TipoPersonaEasy.Text = infobase.Rows[0]["TipoPersona"].ToString();
                RazonSocialLabel.Text = infobase.Rows[0]["RazonSocial"].ToString();
                RFCLabel.Text = infobase.Rows[0]["RFC"].ToString();
                CorreoLabel.Text = infobase.Rows[0]["correo"].ToString();
                MetodoPago.Text = infobase.Rows[0]["Metodo Pago"].ToString();
                UsoCFDIClaveLabel.Text = infobase.Rows[0]["UsoCFDIClave"].ToString();
                UsoCFDIDesLabel.Text = infobase.Rows[0]["UsoCDFIDes"].ToString();
                TipoPersonaLabel.Text = infobase.Rows[0]["TipodePersona"].ToString();
                RegimenLabelClave.Text = infobase.Rows[0]["RegimenFiscalClave"].ToString();
                RegimenLabelDes.Text = infobase.Rows[0]["RegimenFiscalDes"].ToString();
            }
            else
            {
                TipoPersonaEasy.Text = infobase.Rows[rowposition]["TipoPersona"].ToString();
                RazonSocialLabel.Text = infobase.Rows[rowposition]["RazonSocial"].ToString();
                RFCLabel.Text = infobase.Rows[rowposition]["RFC"].ToString();
                CorreoLabel.Text = infobase.Rows[rowposition]["correo"].ToString();
                MetodoPago.Text = infobase.Rows[rowposition]["Metodo Pago"].ToString();
                UsoCFDIClaveLabel.Text = infobase.Rows[rowposition]["UsoCFDIClave"].ToString();
                UsoCFDIDesLabel.Text = infobase.Rows[rowposition]["UsoCDFIDes"].ToString();
                TipoPersonaLabel.Text = infobase.Rows[rowposition]["TipodePersona"].ToString();
                RegimenLabelClave.Text = infobase.Rows[rowposition]["RegimenFiscalClave"].ToString();
                RegimenLabelDes.Text = infobase.Rows[rowposition]["RegimenFiscalDes"].ToString();
            }

        }


    }
}
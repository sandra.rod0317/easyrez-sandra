﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Easy_Test
{
    public partial class InsertarEasyRez : System.Web.UI.Page
    {
        
        string ConexionBase = @"Server=localhost;Database=easyrez-sandra;Uid=root;Pwd=SandraRod1601";
        protected void Page_Load(object sender, EventArgs e)
        {

            RegimenFiscal();
            CFDI();
            
        }
        //Funcion Para insertar Entidad
        protected void Guardar(object sender, EventArgs e)
        
            {
            //Inicio Insertado
            string SucursalCheck = "No";
;            if (SucursalRadio.Checked) { SucursalCheck = "Si"; }
            using (MySqlConnection conexionsql = new MySqlConnection(ConexionBase))
            {
                //Abriendo Conexion
                conexionsql.Open();
                MySqlCommand sqlcommand = new MySqlCommand("InsertarEntidad", conexionsql);
                sqlcommand.CommandType = CommandType.StoredProcedure;
                sqlcommand.Parameters.AddWithValue("_mail", TextCorreo.Text);
                sqlcommand.Parameters.AddWithValue("_IdTipo", Convert.ToInt32(Tpersona.SelectedValue));
                sqlcommand.Parameters.AddWithValue("_RFC", RFCText.Text);
                sqlcommand.Parameters.AddWithValue("_RazonSocial", RazonSocialText.Text);
                sqlcommand.Parameters.AddWithValue("IdMetodoPago", 6);
                sqlcommand.Parameters.AddWithValue("IdUsoCFDI", Convert.ToInt32( CFDILista.SelectedValue));
                sqlcommand.Parameters.AddWithValue("IDregimen", Convert.ToInt32(RegimenFiscalLista.SelectedValue));
                sqlcommand.Parameters.AddWithValue("Sucursal", SucursalCheck);
                sqlcommand.Parameters.AddWithValue("TipoEnTribu", Convert.ToInt32(TipoETributaria.SelectedValue));
                sqlcommand.ExecuteNonQuery();


            }
        }
        public void CFDI()
        {
            //Búsqueda CFDI
            DataTable infobase = new DataTable();
            using (MySqlConnection sqlcon = new MySqlConnection(ConexionBase))
            {
               //Abriendo conexión a BD
                sqlcon.Open();
                MySqlDataAdapter sqlda = new MySqlDataAdapter("BusquedaCFDI", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.AddWithValue("idclas", Convert.ToInt32(117));

                sqlda.Fill(infobase);
                for (int i = 0; i <= infobase.Rows.Count - 1; i++)
                {
                    CFDILista.Items.Add(new ListItem(infobase.Rows[i]["Clave"].ToString() + " - " + infobase.Rows[i]["Descripción"].ToString(), infobase.Rows[i]["idArtefactoCFDI"].ToString()));
                }

            }
        }
        public void RegimenFiscal()
        {
            //Búsqueda Regimen Fiscal
            using (MySqlConnection sqlcon = new MySqlConnection(ConexionBase))
            {
                //LLenado y búsqueda de Regimen Fiscal
                DataTable infobase = new DataTable();
                sqlcon.Open();
                MySqlDataAdapter sqlda = new MySqlDataAdapter("Busqueda_Regimen", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.AddWithValue("idclas", Convert.ToInt32(117));

                sqlda.Fill(infobase);
                for (int i = 0; i <= infobase.Rows.Count - 1; i++)
                {
                    RegimenFiscalLista.Items.Add(new ListItem(infobase.Rows[i]["Clave"].ToString() + " - " + infobase.Rows[i]["Descripción"].ToString(), infobase.Rows[i]["idArtefactoCFDI"].ToString()));
                }

            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Easy-RezConsulta.aspx.cs" Inherits="Easy_Test.Easy_RezConsulta" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display&display=swap" rel="stylesheet">
    <title>Consulta Easy-Rez</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" href="Resources/css/bootstrap.min.css" rel="stylesheet" />

    <link href="Resources/css/Easyprincipal.css" rel="stylesheet" />

    <!-- Custom stlylesheet -->

    <link type="text/css" href="Resources/css/style.css" rel="stylesheet" />


</head>

<body>
    <div id="booking" class="section">
        <div class="section-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="booking-cta">
                            <h1>TEST EASY REZ</h1>

                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="booking-form">
                            <form id="ConsultaEasy" runat="server">
                                <div class="">
                                    <div class="">
                                        <h2>Sistema Prueba Easy-Rez</h2>
                                        <br />
                                        <%--Listas--%>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:Label runat="server" class="eazytext" Text="Buscar por"></asp:Label>
                                                <asp:DropDownList runat="server" ID="BusquedaTipo" OnSelectedIndexChanged="AnteriorEazy" AutoPostBack="false">
                                                    <asp:ListItem Value="117">Selecciona </asp:ListItem>
                                                    <asp:ListItem Value="117">Cliente </asp:ListItem>
                                                    <asp:ListItem Value="117">Emisor </asp:ListItem>
                                                    <asp:ListItem Value="117">Proveedor PAC </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label runat="server" class="eazytext" Text="Tipo:"></asp:Label>

                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="TipoPersonaEasy"></asp:Label>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <%--Listas--%>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="Razón Social:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="RazonSocialLabel"></asp:Label>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="RFC:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="RFCLabel"></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="Correo:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="CorreoLabel"></asp:Label>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <%--Etiquetas--%>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="Metodo Pago:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Text="" Class="eazytextres" ID="MetodoPago"></asp:Label>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="UsoCFDI Clave:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="UsoCFDIClaveLabel"></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="UsoCFDI Descripcion:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="UsoCFDIDesLabel"></asp:Label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="TipoPersona:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="TipoPersonaLabel"></asp:Label>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="RegimenFiscal Clave"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="RegimenLabelClave"></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" class="eazytext" Text="RegimenFiscal Descripción:"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" Class="eazytextres" Text="" ID="RegimenLabelDes"></asp:Label>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:Button runat="server" class="boton" OnClick="AnteriorEazy" Text="Anterior"></asp:Button>
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Button runat="server" class="boton" OnClick="SiguienteEazy" Text="Siguiente"></asp:Button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




</body>


</html>
